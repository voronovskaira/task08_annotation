import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class GenericReflection<T> {
    T item;
    Class clazz;
    private static Logger LOG = LogManager.getLogger(GenericReflection.class);

    public GenericReflection(T item) {
        this.item = item;
        this.clazz = item.getClass();
    }

    public void getClassInformation() {
        listAllFields();
        listAllConstructors();
        listAllMethods();
    }

    private void listAllFields() {
        Arrays.stream(clazz.getDeclaredFields()).forEach(field -> {
            LOG.info(getFieldAnnotation(field));
            LOG.info(String.format("%s %s %s;", Modifier.toString(field.getModifiers()), field.getType().getSimpleName(), field.getName()));
        });
    }


    private void listAllConstructors() {
        Arrays.stream(clazz.getDeclaredConstructors()).forEach(constructor -> {
            LOG.info(String.format("%s %s%s", Modifier.toString(constructor.getModifiers()), constructor.getName(), getConstructorParams(constructor)));
        });
    }

    private void listAllMethods() {
        Arrays.stream(clazz.getDeclaredMethods()).forEach(method -> {
            LOG.info(String.format("%s %s %s%s", Modifier.toString(method.getModifiers()), method.getReturnType(), method.getName(), getMethodParams(method)));
        });
    }


    private String getMethodParams(Method method) {
        StringBuilder builder = new StringBuilder();
        builder.append("(");
        Arrays.stream(method.getParameters()).forEach(parameter -> builder.append(parameter.toString()));
        builder.append(")");
        return builder.toString();
    }

    private String getFieldAnnotation(Field field) {
        StringBuilder builder = new StringBuilder();
        Arrays.stream(field.getDeclaredAnnotations()).forEach(annotation -> {
            builder.append(annotation.toString());
        });
        return builder.toString();
    }

    private String getConstructorParams(Constructor method) {
        StringBuilder builder = new StringBuilder();
        builder.append("(");
        Arrays.stream(method.getParameters()).forEach(parameter -> builder.append(parameter.toString()));
        builder.append(")");
        return builder.toString();

    }
}
