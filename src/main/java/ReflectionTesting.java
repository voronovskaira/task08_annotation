import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class ReflectionTesting {
    private Class clazz;
    private static Logger LOG = LogManager.getLogger(ReflectionTesting.class);

    public ReflectionTesting(Class clazz) {
        this.clazz = clazz;
    }

    public void listAllFieldsWithAnnotation() {
        List<Field> fields = new ArrayList<>(Arrays.asList(clazz.getDeclaredFields()));
        fields.stream().filter(field -> !Objects.isNull(field.getAnnotation(OwnAnnotation.class))).forEach(LOG::info);
    }

    public void listAnnotationValues() {
        List<Field> fields = new ArrayList<>(Arrays.asList(clazz.getDeclaredFields()));
        fields.stream().filter(field -> !Objects.isNull(field.getAnnotation(OwnAnnotation.class))).forEach(field -> Arrays.stream(field.getDeclaredAnnotations()).forEach(ann -> LOG.info(ann.toString())));
    }

    public void invokeShowStringMethod() {
        try {
            Method showStringMethod = clazz.getDeclaredMethod("showString", String.class, String.class);
            showStringMethod.setAccessible(true);
            Object var = showStringMethod.invoke(new TestClass(), "Hello", "World");
            LOG.info(var);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void invokeGetPrivateFieldMethod(int value) {
        try {
            TestClass testObject = new TestClass();
            Method setPrivateFieldMethod = clazz.getMethod("setPrivateField", int.class);
            setPrivateFieldMethod.invoke(testObject, value);
            Method getPrivateFieldMethod = clazz.getMethod("getPrivateField");
            Object privateValue = getPrivateFieldMethod.invoke(testObject);
            LOG.info(privateValue);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void invokeVarArgsMethod() {
        try {
            TestClass testObject = new TestClass();
            Method varArgsMethod = clazz.getMethod("varArgsMethod", String[].class);
            varArgsMethod.invoke(testObject, (Object) new String[]{"Hello ", " world", " from ", " varargs", " method"});
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

    }

    public void invokeVarArgsMethodWithString() {
        try {
            TestClass testObject = new TestClass();
            Method varArgsMethod = clazz.getMethod("varArgsMethodWithString", String.class, int[].class);
            varArgsMethod.invoke(testObject, "Var arg", new int[]{1, 7, 2, 3, 5, 4});
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

    }
}
