import java.util.Arrays;

public class TestClass {
    @OwnAnnotation(name = "private field")
    private int privateField;

    @OwnAnnotation(name = "protected field")
    protected int protectedField;

    @OwnAnnotation(name = "public field")
    public String publicField;

    public int withoutAnnotation;

    public TestClass(int privateField, int protectedField, String publicField, int withoutAnnotation) {
        this.privateField = privateField;
        this.protectedField = protectedField;
        this.publicField = publicField;
        this.withoutAnnotation = withoutAnnotation;
    }

    public TestClass() {
    }

    public int getPrivateField() {
        return privateField;
    }

    public void setPrivateField(int privateField) {
        this.privateField = privateField;
    }

    private String showString(String a, String b){
        return a.concat(b);
    }

    public void varArgsMethod(String... vars){
        System.out.println(Arrays.toString(vars));
    }

    public void varArgsMethodWithString(String param, int... vars){
        System.out.println(param);
        System.out.println(Arrays.toString(vars));
    }
}
